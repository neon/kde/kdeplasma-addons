Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kdeplasma-addons
Source: git://anongit.kde.org/kdeplasma-addons

Files: *
Copyright: 2013, Aaron Seigo <aseigo@kde.org>
           2013-2015, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2008, Alessandro Diaferia <alediaferia@gmail.com>
           2007, Alexis Ménard <darktears31@gmail.com>
           2007, André Duffeck <duffeck@kde.org>
           2007-2008, Anne-Marie Mahfouf <annma@kde.org>
           2007, Antonio Vinci <mercurio@personellarete.it>
           2014, Ashish Madeti <ashishmadeti@gmail.com>
           2013, Aurélien Gâteau <agateau@kde.org>
           2009, Ben Boeckel <MathStuf@gmail.com>
           2015, Bernhard Friedreich <friesoft@gmail.com>
           2013, Bhushan Shah <bhush94@gmail.com>
           2009-2010, Björn Ruberg <bjoern@ruberg-wegener.de>
           2011-2014, Weng Xuetian <wengxt@gmail.com>
           2007, Chani Armitage <chanika@gmail.com>
           2008-2010, Christian Weilbach <dunsens@web.de>
           2012-2014, David Edmundson <davidedmundson@kde.org >
           2014, David Edmundson <davidedmundson@kde.org>
           2007-2014, Davide Bettio <davide.bettio@kdemail.net>
           2014, Eike Hein <hein@kde.org>
           2011, Federico Zenith <federico.zenith@member.fsf.org>
           1989-1991, Free Software Foundation, Inc
           2008, Georges Toth <gtoth@trypill.org>
           2008, Gerhard Gappmeier <gerhard.gappmeier@ascolab.com>
           2013, Heena Mahour <heena393@gmail.com>
           2007, Henry Stanaland <stanaland@gmail.com>
           2008, Ian Monroe <ian@monroe.nu>
           2007-2010, Ivan Cukic <ivan.cukic@kde.org>
           2014, Jeremy Whiting <jpwhiting@kde.org>
           2007, Jesper Thomschutz <jesperht@yahoo.com>
           2014, Kai Uwe Broulik <kde@privat.broulik.de>
           2012, Luiz Romário Santana Rios <luizromario@gmail.com>
           2012, Luís Gabriel Lima <lampih@gmail.com>
           2015, Marco Martin <mart@kde.org>
           2007-2009, Marco Martin <notmart@gmail.com>
           2011-2013, Martin Gräßlin <mgraesslin@kde.org>
           2013, Martin Klapetek <mklapetek@kde.org>
           2014, Martin Yrjölä <martin.yrjola@gmail.com>
           2008-2012, Matthias Fuchs <mat69@gmx.net>
           2008-2010, Michal Dutkiewicz aka Emdek <emdeck@gmail.com>
           2014, Mikhail Ivchenko <ematirov@gmail.com>
           2008, Montel Laurent <montel@kde.org>
           2008, Nick Shaforostoff <shaforostoff@kde.ru>
           2008-2009, Olivier Goffart <ogoffart@kde.org>
           2008, Peter ZHOU <peterzhoulei@gmail.com>
           2007-2009, Petri Damstén <damu@iki.fi>
           2007, Pino Toscano <pino@kde.org>
           2012, Reza Fatahilah Shah <rshah0385@kireihana.com>
           2007, Riccardo Iaconelli <riccardo@kde.org>
           2008, Rob Scheepmaker <r.scheepmaker@student.utwente.nl>
           2010, Ruslan Nigmatullin <euroelessar@ya.ru>
           2008, Ryan P. Bitanga <ryan.bitanga@gmail.com>
           2009-2013, Sebastian Kügler <sebas@kde.org>
           2011-2012, Shaun Reich <shaun.reich@kdemail.net>
           2007-2009, Shawn Starr <shawn.starr@rogers.com>
           2005-2007, Siraj Razick <siraj@kde.org>
           1998-2000, Stephan Kulow <coolo@kde.org>
           2008, Thomas Coopman <thomas.coopman@gmail.com>
           2007, Thomas Georgiou <TAGeorgiou@gmail.com> and Jeff Cooper <weirdsox11@gmail.com>
           2007, Tobias Koenig <tokoe@kde.org>
           2009, Wang Hoi <zealot.hoi@gmail.com>
           2007-2011, Free Software Foundation, Inc
           2007-2010, Ivan Čukić
           2010-2012, Jason A. Donenfeld <Jason@zx2c4.com>
           2009, K Desktop Environment
           2008, Rosetta Contributors and Canonical Ltd 2008
           2009, Rosetta Contributors and Canonical Ltd 2009
           2010, Rosetta Contributors and Canonical Ltd 2010
           2011, Rosetta Contributors and Canonical Ltd 2011
           2012, Rosetta Contributors and Canonical Ltd 2012
           2014, Rosetta Contributors and Canonical Ltd 2014
           2007-2015, This_file_is_part_of_KDE
License: GPL-2+

Files: debian/*
Copyright: 2007-2009, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2014-2015, Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
License: GPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2', likewise, the
 complete text of the GNU General Public License version 3 can be found in
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Library General Public License version 2 as published by
 the Free Software Foundation
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details
 .
 You should have received a copy of the GNU Library General Public License
 along with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 -
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 -
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 -
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2.1+3+KDEeV
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) version 3, or any
 later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 -
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1', likewise,
 the complete text of the GNU Lesser General Public License version 3 can be
 found in `/usr/share/common-licenses/LGPL-3'.
